//go test -coverprofile=cover.out && go tool cover  -html=cover.out -o cover.html

// make sure to execute `go install` before tests
package main

import (
	"bytes"
	"flag"
	"io"
	"os"
	"testing"
	"reflect"
)

var csvFilename = flag.String("csvFilename", "", "dir of package containing embedded files")

func TestGetProblemsFromFile(t *testing.T) {
	t.Log(*csvFilename)
	var filename string
	var err error

	oldOsExit := osExit
	defer func() { osExit = oldOsExit }()
	var got int
	myExit := func(code int) {
		got = code
	}

	osExit = myExit

	filename = "invalid-file.csv"
	output := captureStdout(func() {
		_ = getProblemsFromFile(&filename)
	})
	//_ = getProblemsFromFile(&filename)
	if exp := 1; got != exp {
		t.Errorf("Expected exit code: %d, got: %d", exp, got)
	}
	//if err == nil {
	//	t.Errorf("Expected error, but got: %s", err)
	//}

	expErrMsg := "Failed to open CSV file: " + filename + "\n"
	if output != expErrMsg {
		t.Errorf("Expected error message %q, but got: %q", expErrMsg, output)
	}
	if exp := 1; got != exp {
		t.Errorf("Expected exit code: %d, got: %d", exp, got)
	}

	filename = "problems_invalid_test.csv"
	_ = getProblemsFromFile(&filename)
	//if err == nil {
	//	t.Errorf("Expected error, but got: %s", err)
	//}

	//expErrMsg = "Failed to parse the provided CSV file."
	//if err.Error() != expErrMsg {
	//	t.Errorf("Expected error message %q, but got: %q", expErrMsg, err.Error())
	//}

	filename = "problems_test.csv"
	res := getProblemsFromFile(&filename)
	if err != nil {
		t.Errorf("Expected success, but got error: %s", err)
	}

	expRes := []problem{{"5+5", "10"}, {"1+1", "2"}}
	if len(res) != len(expRes) || res[0] != expRes[0] || res[1] != expRes[1] {
		t.Errorf("Expected result %q, but got: %q", expRes, res)
	}

}

func TestRunQuiz(t *testing.T) {
	tu := 1
	runQuiz([]problem{{"5+5", "10"}, {"1+1", "2"}}, &tu)
	tu = 0
	runQuiz([]problem{{"5+5", "10"}, {"1+1", "2"}}, &tu)
}

func TestCheckAnswer(t *testing.T) {
	correct := 0
	exp := 1
	p := problem{"0+1", "1"}
	correct = checkAnswer("1", p, correct)
	p = problem{"0+1", "0"}
	correct = checkAnswer("1", p, correct)
	if correct != 1 {
		t.Errorf("Expected %d correct answer in total, but got: %d", exp, correct)
	}
}

func TestParseLines(t *testing.T) {
	lines := [][]string{{"5+5","10"},{"1+1","2"}}
	exp := []problem{{"5+5","10"},{"1+1","2"}}
	got := parseLines(lines)
	if !reflect.DeepEqual(exp, got) {
		t.Errorf("Expected result: %v, but got: %v", exp, got)
	}
}

func TestExit(t *testing.T) {
	oldOsExit := osExit
	defer func() { osExit = oldOsExit }()
	var got int
	msg := "Test Exit Message"
	myExit := func(code int) {
		got = code
	}
	osExit = myExit
	output := captureStdout(func() {
		exit(msg)
	})
	if exp := 1; got != exp {
		t.Errorf("Expected exit code: %d, got: %d", exp, got)
	}
	if exp := msg + "\n"; exp != output {
		t.Errorf("Expected output %q, but got: %q", exp, output)
	}
}

func TestMainFunc(t *testing.T) {
	exp := `Problem #1: 5+5 = Problem #2: 1+1 = Problem #3: 8+3 = Problem #4: 1+2 = Problem #5: 8+6 = Problem #6: 3+1 = Problem #7: 1+4 = Problem #8: 5+1 = Problem #9: 2+3 = Problem #10: 3+3 = Problem #11: 2+4 = Problem #12: 5+2 = You scored 0 out of 12.
press Enter to exit quiz
`
	output := captureStdout(func() {
		main()
	})
	if exp != output {
		t.Errorf("Expected output %q, but got: %q", exp, output)
	}
}

// not thread safe
func captureStdout(f func()) string {
	old := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	f()

	w.Close()
	os.Stdout = old

	var buf bytes.Buffer
	io.Copy(&buf, r)
	return buf.String()
}
